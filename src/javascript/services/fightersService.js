import { callApi } from '../helpers/apiHelper';

class FighterService {
  #endpoint = 'fighters.json';

  async getFighters() {
    try {
      const apiResult = await callApi(this.#endpoint);
      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    // todo: implement this method
    const endpoint = `details/fighter/${id}.json`
    const currentFighter = await callApi(endpoint)
    return currentFighter
    // endpoint - `details/fighter/${id}.json`;
  }
}

export const fighterService = new FighterService();
