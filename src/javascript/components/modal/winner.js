export function showWinnerModal(fighter) {
  document.body.insertAdjacentHTML('beforebegin', `
    <div class='modal-layer'>
      <div class='modal-root winner-modal'>
        <div class='modal-header winner-modal__header'>
          <img class='winner-modal__img' src='${ fighter.source }' />
          ${ fighter.name.toUpperCase() } WIN!
        </div>
        <div class='modal-body winner-modal__body'>        
          <button class='winner-modal__restart'>REMATCH</button>
        </div>
      </div> 
     </div>
  `);

  const restartButton = document.querySelector('.winner-modal__restart')

  restartButton.addEventListener('click', () => {
    location.reload()
  })
}
