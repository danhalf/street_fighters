import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${ positionClassName }`
  });

  if(!fighter){
    return fighterElement
  }

  const fighterImage = createFighterImage(fighter)
  const fighterInfo = createElement({tagName: 'ul', className: 'fighter-preview___info'})
  const infoBlock = createFighterInfo(fighter)

  fighterInfo.append(...infoBlock)
  fighterElement.append(fighterImage, fighterInfo)

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;

  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };

  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighterInfo) {
  const isCurrentFighter = fighterId => fighterId !== '_id' && fighterId !== 'source'
  return Object.keys(fighterInfo)
    .filter(isCurrentFighter)
    .map(fighter => {
      const fighterInfoItem = createElement({tagName: 'li', className: 'fighter-preview___info-item'})
      fighterInfoItem.innerHTML = `${getIcon(fighter)} ${fighterInfo[fighter]}`
      return fighterInfoItem
    })
}

const getIcon = icon => {
  switch (icon) {
    case 'name':
      return '&#x1F458;'
    case 'attack':
      return '&#x2694;&#xFE0F'
    case 'health':
      return '&#128151;'
    case 'defense':
      return '&#x1F6E1;&#xFE0F;'
    default:
      return icon
  }
}