import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  const CRITICAL_HIT_DELAY = 10000;
  const pressedKeys = new Set();

  class Fighter {
    constructor(fighter, selector) {
      this.fighter = fighter;
      this.maxHP = this.fighter.health;
      this.currentHP = this.fighter.health;
      this.attack = this.fighter.attack;
      this.block = false;
      this.indicator = document.getElementById(selector);
    }
  }


  return new Promise((resolve) => {
    const FIRST_FIGHTER = new Fighter(firstFighter, 'left-fighter-indicator');
    const SECOND_FIGHTER = new Fighter(secondFighter, 'right-fighter-indicator');

    document.addEventListener('keydown', (event) => {

      pressedKeys.add(event.code);

      if (!event.repeat) {
        switch (event.code) {
          case controls.PlayerOneAttack:
            if (!pressedKeys.has(controls.PlayerOneBlock) && !pressedKeys.has(controls.PlayerTwoBlock)) {
              hit(FIRST_FIGHTER, SECOND_FIGHTER);
            }
            break;
          case controls.PlayerTwoAttack:
            if (!pressedKeys.has(controls.PlayerTwoBlock) && !pressedKeys.has(controls.PlayerOneBlock)) {
              hit(SECOND_FIGHTER, FIRST_FIGHTER);
            }
            break;
        }
      }


      if (!FIRST_FIGHTER.block &&
        controls.PlayerOneCriticalHitCombination.includes(event.code) &&
        checkCriticalKeysPressed(controls.PlayerOneCriticalHitCombination, pressedKeys)) {
        hit(FIRST_FIGHTER, SECOND_FIGHTER, true);
      }
      if (!SECOND_FIGHTER.block &&
        controls.PlayerTwoCriticalHitCombination.includes(event.code) &&
        checkCriticalKeysPressed(controls.PlayerTwoCriticalHitCombination, pressedKeys)) {
        hit(SECOND_FIGHTER, FIRST_FIGHTER, true);
      }
    });

    document.addEventListener('keyup', (event) => {
      pressedKeys.delete(event.code);
    });


    function hit(attackerFighter, victimFighter, critical = false) {
      let damage = null;
      if (critical) {
        damage = getCriticalHit(attackerFighter.fighter);
        attackerFighter.block = true;
        setTimeout(() => {
          attackerFighter.block = false;
        }, CRITICAL_HIT_DELAY);
      } else {
        damage = getDamage(attackerFighter.fighter, victimFighter.fighter);
      }
      victimFighter.currentHP -= damage;
      changeFighterHealthBar(victimFighter);
      if (victimFighter.currentHP <= 0) resolve(attackerFighter.fighter);
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  let damage = hitPower - blockPower;
  return damage > 0 ? damage : 0;
}

export const getCriticalHit = fighter => fighter.attack * 2;

export const getHitPower = fighter  => (Math.random() + 1) * fighter.attack

export const getBlockPower = fighter  => (Math.random() + 1) * fighter.defense

const changeFighterHealthBar = fighter => {
  let percent = fighter.currentHP * 100 / fighter.maxHP;
  if (percent < 0) percent = 0;
  fighter.indicator.style.width = `${ percent }%`;
}

const checkCriticalKeysPressed = (keys, pressed) => {
  for (let key of keys) {
    if (!pressed.has(key)) {
      return false;
    }
  }
  return true;
}